# nodejs-mongoose-express-api
Please follow the steps below before performing this task
<br>

1. Clone the project with this command.(You must have git installed on your PC in need to do that. If not, click this link to install.)
https://www.digitalocean.com/community/tutorials/how-to-install-git-on-ubuntu-20-04

```
git Clone https://gitlab.com/harshkagathara/node-js-practical-biztech-task-02.git
```

2. After the clone is complete, use this command to open the folder.

```
cd node-js-practical-biztech-task-02
```


3. The node package can be installed with the following command.

```
npm install
```
4. To run the project, give this command.

```
npm run dev
```

You created the.env file by yourself. You can add the variables DB_Port, DB_Name, and Port in it.
