const mongoose = require('mongoose')

const couponSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: false
  },
  expirationDate: {
    type: Date,
    required: false
  },
  location: {
    type: String,
    required: true
  }
}, {
  timestamps: true
})

const couponInfo = mongoose.model('coupon', couponSchema)
module.exports = couponInfo
