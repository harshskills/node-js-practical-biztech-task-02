const couponModel = require('../models/coupon.model')
const { validationResult } = require('express-validator')

exports.CreateCoupon = async (req, res, next) => {
  try {
    const errors = validationResult(req)

    if (!errors.isEmpty()) {
      res.status(422).json({ errors: errors.array() })
      return
    }
    const datas = await couponModel.CreateCoupon(req.body)
    res.json(datas)
  } catch (err) {
    return next(err)
  }
}

exports.FindByLocationName = async (req, res, next) => {
  try {
    const datas = await couponModel.FindByLocationName(req.params.location)
    res.json(datas)
  } catch (err) {
    return next(err)
  }
}
