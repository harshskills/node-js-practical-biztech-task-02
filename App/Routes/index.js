module.exports = (app) => {
  const couponController = require('../controllers/coupon.controller')
  const validation = require('../middlewares/validation')

  app.post('/createCoupon', validation.validate('createCoupon'), couponController.CreateCoupon)
  app.get('/FindByLocationName/:location', couponController.FindByLocationName)
}
