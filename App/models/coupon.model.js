const CouponSchema = require('../schemas/coupon')

class Coupons {
  async CreateCoupon (couponDatas) {
    const promise = new Promise(function (resolve, reject) {
      new CouponSchema(couponDatas).save((err, doc) => {
        if (!err) {
          resolve(doc)
        } else {
          reject(err)
          console.log('Error during saving coupon : ' + err)
        }
      })
    })
    return promise
  }

  async FindByLocationName (locationName) {
    const promise = new Promise(function (resolve, reject) {
      CouponSchema.find({ location: locationName }, (err, doc) => {
        if (!err) resolve({ results: doc })
        else {
          console.error('Error during find coupon datas : ' + err)
        }
      })
    })
    return promise
  }
}

const indexController = new Coupons()
module.exports = indexController
