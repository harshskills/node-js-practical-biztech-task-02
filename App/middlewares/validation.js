const { body } = require('express-validator')

exports.validate = (method) => {
  switch (method) {
    case 'createCoupon': {
      return [
        body('name', "name doesn't exists").exists(),
        body('location', "location doesn't exists").exists()
      ]
    }
  }
}
