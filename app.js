const express = require('express')
const bodyParser = require('body-parser')

require('dotenv').config()
const app = express()

require('./App/DB')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

require('./App/Routes')(app)
const Port = process.env.Port || 3000
app.listen(Port, () => {
  console.log(`Server Listing at :- ${Port}`)
})
